#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "list.h"

//TODO free data of Element in addition to element itself
//TODO change so data reads out by pointer (still over return possible)

void initiateList(list *l, int sizeOfElement){
	l->first = NULL;
	l->last = NULL;
	l->length = 0;
	l->sizeOfElement = sizeOfElement;
}


void* save(void *ptr, int Msize){

	void *Vptr = malloc(Msize);

	char *ptrc, *ptrc2;

	ptrc = (char *) ptr;
	ptrc2 = (char *) Vptr;

	for(int i = 0; i < Msize; i++){
		*(ptrc2 + i) = *(ptrc + i);
	}
	
	return Vptr;
}

void addToList(list *l, void *value){
	listElement *newEl = malloc(sizeof(listElement));
	if(l->length == 0){
		l->first = newEl;
		newEl->prev = NULL;
	} else {
		l->last->next = newEl;
		newEl->prev = l->last;
	}
	newEl->next = NULL;
	newEl->data = save(value, l->sizeOfElement);
	l->last = newEl;
	l->length++;
}

int getListLength(list *l){
	return l->length;
}

void printAddresses(list *l){
	listElement *le = l->first;
	if(le){
		while(le->next){
			printf("pointer: %p\n", le);
			le = le->next;
		}	
		printf("pointer: %p\n", le);
	}
}

int iterateToElementForwards(list *l, int index, listElement **le){
	*le = l->first;
	if(!(*le)){
		return 1;
	}
	for(int i = 0; i < index; i++){
		if((*le)->next){
			*le = (*le)->next;
		}else {
			return 1;
		}
	}
	return 0;
}

int iterateToElementBackwards(list *l, int index, listElement **le, int listLength){
	*le = l->last;
	if(!(*le)){
		return 1;
	}
	for(int i = listLength - 1; i > index; i--){
		if((*le)->prev){
			*le = (*le)->prev;
		}else {
			return 1;
		}
	}
	return 0;
}

int getListElementByIndex(list *l, int index, listElement **le){
	int listLength = getListLength(l);	
	int fromLast = index > listLength / 2;
	if(fromLast){
		if(iterateToElementBackwards(l, index, le, listLength)){
			printf("error\n");
		}	
	}else{
		if(iterateToElementForwards(l, index, le)){
			printf("error\n");
		}

	}
	return 0;
}

void* getFromList(list *l, int index){
	if(l->length == 0){
		return 0;
	}
	listElement *le;
	getListElementByIndex(l, index, &le);
	return le->data;
}

int removeFromList(list *l, int index){
	if(l->length == 0){
		return 1;
	}
	listElement *le;
	getListElementByIndex(l, index, &le);
	if(index == 0){
		le->next->prev = NULL;
		l->first = le->next;
	} else if (index == (l->length - 1)) {
		le->prev->next = NULL;	
		l->last = le->prev;
	} else {
		le->prev->next = le->next;
		le->next->prev = le->prev;
	}
	free(le->data);
	free(le);
	l->length--;
	return 0;	
}

void freeAllElements(list *l){
	listElement *le = l->first;
	if(le){
		while(le->next){
			listElement *leO = le;
			le = le->next;
			free(leO->data);
			free(leO);
		}
		free(le->data);
		free(le);
	}
	l->first = NULL;
	l->length--;
}
