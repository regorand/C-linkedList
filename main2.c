#include <stdio.h>
#include "list.h"

int main(){
	list charList;
	list intList;
	initiateList(&charList, sizeof(char));
	initiateList(&intList, sizeof(int));
	char c1 = 'h', c2 = 'i';	
	
	addToList(&charList, &c1);
	addToList(&charList, &c2);

	for(int i = 10; i < 20; i++){
		addToList(&intList, &i);
	}
	c1 = 'o';
	c2 = 'k';

	printf("\nlength charList: %d\n", getListLength(&charList));
	printf("length intList: %d\n", getListLength(&intList));

	printf("\n----------------\n\n");

	for(int i = 0; i < getListLength(&charList); i++){
		void *result = getFromList(&charList, i);
		printf("charList #%d: %c\n", i, *((char*)result));
	}

	printf("\n----------------\n\n");

	for(int i = 0; i < getListLength(&intList); i++){
		void *result = getFromList(&intList, i);
		printf("intList #%d: %d\n", i, *((int*)result));
	}


	freeAllElements(&charList);
	freeAllElements(&intList);
}
