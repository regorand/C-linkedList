#ifndef LIST_H_
#define LIST_H_

typedef struct listE{
	void *data;
	struct listE *next;
	struct listE *prev;
} listElement;

typedef struct{
	int sizeOfElement;
	listElement *first;
	listElement *last;
	int length;
} list ;

void* saveDataToMemory(void *ptr, int Msize);

void initiateList(list *l, int sizeOfElement);

void addToList(list *l, void *value);

int getListLength(list *l);

void printAddresses(list *l);

int iterateToElementforwards(list *l, int index, listElement **le);

int iterateToElementBackwards(list *l, int index, listElement **le, int listLength);

int getListElementByIndex(list *l, int index, listElement **le);

void* getFromList(list *l, int index);

int removeFromList(list *l, int index);

void freeAllElements(list *l);

#endif
