#include <stdio.h>
#include "list.h"

int main(){
	list list;
	//change initiatelist to also take an int parameter as the memory size of the type, then use that to malloc the correct amount of memory
	initiateList(&list, sizeof(int));
	for(int i = 0; i < 10; i++){
		addToList(&list, &i);
	}

	int length = getListLength(&list);
	printf("length: %d\n", length);
	removeFromList(&list, 9);
	length = getListLength(&list);
	printf("length: %d\n", length);
	for(int i = 0; i < length; i++){
	int *result = getFromList(&list, i);
		printf("#%d: %d\n", i, *((int*)result));
	}
	freeAllElements(&list);
}
